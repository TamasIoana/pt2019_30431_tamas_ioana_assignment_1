package model;

public class Polynomial {
	private int degree; // the degree of the polynomial
	private int[] coeff; // array that holds the coefficients of the polynomial

	public  Polynomial(int[] coeff) {
		this.coeff = coeff;
		degree = coeff.length - 1;
	}
	
	//get the coefficients at a specific position
	public  int getThisCoeff(int position) {
		return coeff[position];

	}
	public  int getDegree() {
		return degree;
	}

	public  void setDegree(int degree) {
		this.degree = degree;
	}


	public  int[] getCoeff() {
		return coeff;
	}

	public  void setCoeff(int[] coeff) {
		this.coeff = coeff;
	}

	
	//format of the polynomial
	public  String toString() {
		if (degree == 0)
			return "" + coeff[0];
		if (degree == 1)
			return coeff[1] + "x + " + coeff[0];
		String s = coeff[degree] + "x^" + degree;
		for (int i = degree - 1; i >= 0; i--) {
			if (coeff[i] == 0)
				continue;
			else if (coeff[i] > 0)
				s = s + " + " + (coeff[i]);
			else if (coeff[i] < 0)
				s = s + " - " + (-coeff[i]);
			if (i == 1)
				s = s + "x";
			else if (i > 1)
				s = s + "x^" + i;
		}
		return s;
	}


	
	
}
