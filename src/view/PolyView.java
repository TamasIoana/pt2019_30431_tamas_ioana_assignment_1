package view;

//import java.awt.BorderLayout;
//import java.awt.FlowLayout;
//import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class PolyView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 Box mainBox, hBox1, hBox2, hBox3, hBox4;
	 JLabel jlP1=new JLabel("  P1: ");
	 public JTextField p1  = new JTextField(10);
	 JLabel jlP2=new JLabel("  P2: ");
	 public JTextField p2 = new JTextField(10);
	 public JButton bplus = new JButton(" +   ");
	 public JButton bminus = new JButton(" -   ");
	 public JButton bmul = new JButton(" * ");
	
	private JLabel jlSol=new JLabel("Result: ");
	public JTextField calcSolution = new JTextField(10);
	
	public PolyView(){
		//JPanel calcPanel = new JPanel();
	
	    this.setTitle("Polynomial Operations");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(500, 170);
		
		mainBox = Box.createVerticalBox();

		hBox1 = Box.createHorizontalBox();
		hBox2 = Box.createHorizontalBox();
		hBox3 = Box.createHorizontalBox();
		hBox4 = Box.createHorizontalBox();
		//hBox5 = Box.createHorizontalBox();
		
		/*
		calcPanel.add(jlP1);
		calcPanel.add(firstNr);
		calcPanel.add(jlP2);
		calcPanel.add(secondNr);
		calcPanel.add(calculateButton);
		calcPanel.add(jlSol);
		calcPanel.add(calcSolution);
		
		add(calcPanel, BorderLayout.CENTER);
		*/
		// End of setting up the components --------
	
		hBox1.add(jlP1);
		hBox1.add(p1);
		hBox2.add(jlP2);
		hBox2.add(p2);
		hBox3.add(Box.createHorizontalStrut(20));
		hBox3.add(bplus);
		hBox3.add(Box.createHorizontalStrut(5));
		hBox3.add(bminus);
		hBox3.add(Box.createHorizontalStrut(5));
		hBox3.add(bmul);
		hBox3.add(Box.createHorizontalStrut(5));
		hBox4.add(jlSol);
		hBox4.add(calcSolution);
		
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox1);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox2);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox3);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox4);
		mainBox.add(Box.createVerticalStrut(5));
	
		
		
		
		add(mainBox);
		
	}
	
	

	 public JLabel getPolynom1() {
	  return jlP1;
	 }
	 
	 public JLabel getPolynom2() {
		  return jlP2;
		 }
		 
	public int getP1(){
		
		return Integer.parseInt(p1.getText());
		
	}
	
	public int getP2(){
		
		return Integer.parseInt(p2.getText());
		
	}
	
	public int getCalcSolution(){
		
		return Integer.parseInt(calcSolution.getText());
		
	}
	
	public void setCalcSolution(int solution){
		
		calcSolution.setText(Integer.toString(solution));
		
	}
	

	// If the calculateButton is clicked execute a method
	// in the Controller named actionPerformed
	
	public void addCalculateListener(ActionListener a){
		
		bplus.addActionListener(a);
		bminus.addActionListener(a);
		bmul.addActionListener(a);
		
	}
	
	
	
	public JButton getButtonPlus() {
		return bplus;
	}
	public JButton getButtonMinus() {
		return bminus;
	}
	public JButton getButtonMul() {
		return bmul;
	}
	
	public JTextField getTextFieldP1() {
		return p1;
	}
	public JTextField getTextFieldP2() {
		return p2;
	}
	
	public JTextField getTextFieldSol() {
		return calcSolution;
	}
	
	

	
}