package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JOptionPane;
import javax.swing.JTextField;


import model.Operations;
import model.Polynomial;
import view.PolyView;


public class PolyController implements ActionListener {

	private PolyView view;
	private Operations model;
	
	//private ActionListener actionListener;
	
	
	public PolyController(PolyView view,Operations model) {
		this.view=view;
		this.model=model;
	
		view.bplus.addActionListener(this);
		view.bminus.addActionListener(this);
		view.bmul.addActionListener(this);
		
		
		
	}
	
	
		public  void actionPerformed(ActionEvent e) {

			int[] firstPolCoeff, secondPolCoeff;
			Operations op = new Operations();
			firstPolCoeff = getPolynomial(view.p1);
			secondPolCoeff = getPolynomial(view.p2);

			//if (e.getSource() == clean) {
				//cleanArea();

			//}
			if (e.getSource() == view.bplus) {
				Polynomial addResult = op.add(new Polynomial(firstPolCoeff), new Polynomial(secondPolCoeff));
				int[] addResultCorrect = addResult.getCoeff();
			
				displayPolynomial(addResultCorrect);
				

			}
		

			else if (e.getSource() == view.bminus) {
				Polynomial subResult = op.sub(new Polynomial(firstPolCoeff),
						new Polynomial(secondPolCoeff));
				int[] subResultCorrect =subResult.getCoeff();
				displayPolynomial(subResultCorrect);
			}

			else if (e.getSource() == view.bmul) {
				Polynomial mulResult = op.multiply(new Polynomial(firstPolCoeff),
						new Polynomial(secondPolCoeff));
				int[] mulResultCorrect = mulResult.getCoeff();
				displayPolynomial(mulResultCorrect);
			}

			
		}
    


		/*public  void cleanArea() {
			pol1.setText("");
			pol2.setText("");
			result.setText("");
		}
*/
		public  int[] getPolynomial(JTextField polyn) {
			int[] pol = null;
			try {
				String text = (polyn.getText()).trim();
				String[] splitActionCommand = text.split(" ");
				pol = new int[splitActionCommand.length];

				for (int i = 0; i < splitActionCommand.length; i++) {
					pol[i] = Integer.parseInt(splitActionCommand[i]);
					//System.out.println(pol[i]);
				}

			} catch (NumberFormatException exception) {
				int messageType = JOptionPane.PLAIN_MESSAGE;
				JOptionPane.showMessageDialog(null, "Please enter only integers!", "Error!", messageType);
			}
			return pol;
		}
		
		public  void displayPolynomial(int[] res) {
			
			Polynomial correctPolynomial = new Polynomial(res);
			view.calcSolution.setText(correctPolynomial.toString());
		}
		
		
	
		
	}

