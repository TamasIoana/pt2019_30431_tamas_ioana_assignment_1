package jUnit;


import model.Polynomial;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OperationsTest {

	int[] a = new int[]{ 1, 2, 4};
	int[] b = new int[]{ 1, 1, 1};
	int[] c=  new int[]{ 3, 2, 4};
	
	Polynomial p = new Polynomial(a);
	Polynomial p1 = new Polynomial(b);
	Polynomial p2= new Polynomial(c);
	
	
	
	@Test
	public final void testAdd() {
	     
		assertEquals("5x^2 + 3x + 2", add(p,p1 ).toString());
      
		
	}
	
	@Test
	public final void testSub() {
		assertEquals("3x^2 + 1x + 2", sub(p2,p1).toString());
		
	}
	 @Test
	 public final void testMultiply() {
		 assertEquals("4x^4 + 6x^3 + 7x^2 + 3x + 1", multiply(p,p1).toString());
		 
	 }


	public Polynomial add(Polynomial p1, Polynomial p2) {
		//array of coefficients of the result polynomial
		int[] res;
		 
		int i=p1.getCoeff().length-1;
		int j=p2.getCoeff().length-1;
		
		//if degree of first polynomial is smaller
		if(p1.getCoeff().length < p2.getCoeff().length) {
			res=new int[p2.getCoeff().length];//res has the same length as p2
			for(int k=0;k<j-i;k++)
				res[k]=p2.getThisCoeff(k);//place in res the coefficients in p2 with higher degree than p1
			for(int k=j-i;k<p2.getCoeff().length;k++)
				//perform addition between the rest of the coefficients 
				res[k]=p2.getThisCoeff(k)+p1.getThisCoeff(k-j+i);
			//return a polynomial with coefficients res
			return new Polynomial(res);		
		}
		
		//if degree of first polynomial is greater 
		else if (p1.getCoeff().length>p2.getCoeff().length) {
			res=new int[p1.getCoeff().length];//res has the same length as p1
		for(int k=0;k<i-j;k++)
			res[k]=p2.getThisCoeff(k);//place in res the coefficients 
		for(int k=i-j;k<p2.getCoeff().length;k++)
			res[k]=p2.getThisCoeff(k)+p2.getThisCoeff(k-i+j);
		return new Polynomial(res);
		}
		else
			
			//if the polynomials have the same degree
		{
			res=new int[p2.getCoeff().length];
			for(int k=0;k<p2.getCoeff().length;k++)
				res[k]=p1.getThisCoeff(k)+p2.getThisCoeff(k);
			
			return new Polynomial(res);	
		}	
	}
	
	public Polynomial sub(Polynomial p1,Polynomial p2) {
		int[] res;
		int i=p1.getCoeff().length-1;
		int j=p2.getCoeff().length-1;
		//the degree of the first polynomial must be greater
		if(p1.getCoeff().length<p2.getCoeff().length) {
			throw new ArithmeticException("Degree of first polynomial must be greater!");
		}
		if(p1.getCoeff().length>p2.getCoeff().length) {
			res=new int[p1.getCoeff().length];
			//place in res the coefficients with greater degree
			for(int k=0;k<i-j;k++)
				res[k]=p1.getThisCoeff(k);
			//perform subtraction between the rest of the coefficients
			for(int k=i-j;k<p1.getCoeff().length;k++)
				res[k]=p1.getThisCoeff(k)-p2.getThisCoeff(k);
				
			return new Polynomial(res);
		}
		else 
			//if they have the same degree
		{
			res=new int[p2.getCoeff().length];
			for(int k=0;k<p2.getCoeff().length;k++)
				//perform subtraction between the coefficients 
				res[k]=p1.getThisCoeff(k)-p2.getThisCoeff(k);
			
			return new Polynomial(res);	
	
		}
	}
	

	public Polynomial multiply(Polynomial p1,Polynomial p2) {
		Polynomial res=null;
		//the result will have degree equal with the sum between degrees of both polynomials
		int[] result=new int[p1.getDegree()+p2.getDegree()+1];
		//perform multiplication between all coefficients no matter the degree
		for (int i=0;i<=p1.getDegree();i++)
			for(int j=0;j<=p2.getDegree();j++)
				result[i+j]+=p1.getThisCoeff(i)*p2.getThisCoeff(j);
		
		res=new Polynomial(result);
		return res;
		
	}
	
	
}
